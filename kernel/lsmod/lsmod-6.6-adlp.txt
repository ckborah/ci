Module                  Size  Used by
cpuid                  12288  0
xt_conntrack           12288  1
nft_chain_nat          12288  3
xt_MASQUERADE          16384  1
nf_nat                 61440  2 nft_chain_nat,xt_MASQUERADE
nf_conntrack_netlink    57344  0
nf_conntrack          204800  4 xt_conntrack,nf_nat,nf_conntrack_netlink,xt_MASQUERADE
nf_defrag_ipv6         24576  1 nf_conntrack
nf_defrag_ipv4         12288  1 nf_conntrack
xfrm_user              57344  1
xfrm_algo              20480  1 xfrm_user
xt_addrtype            12288  2
nft_compat             20480  4
nf_tables             364544  57 nft_compat,nft_chain_nat
nfnetlink              20480  4 nft_compat,nf_conntrack_netlink,nf_tables
br_netfilter           32768  0
bridge                401408  1 br_netfilter
stp                    12288  1 bridge
llc                    16384  2 bridge,stp
overlay               196608  0
intel_uncore_frequency    16384  0
intel_uncore_frequency_common    16384  1 intel_uncore_frequency
sunrpc                802816  1
snd_sof_pci_intel_tgl    12288  0
snd_sof_intel_hda_common   208896  1 snd_sof_pci_intel_tgl
soundwire_intel        73728  1 snd_sof_intel_hda_common
binfmt_misc            24576  1
soundwire_generic_allocation    12288  1 soundwire_intel
snd_sof_intel_hda_mlink    45056  2 soundwire_intel,snd_sof_intel_hda_common
soundwire_cadence      40960  1 soundwire_intel
snd_sof_intel_hda      24576  1 snd_sof_intel_hda_common
snd_sof_pci            24576  2 snd_sof_intel_hda_common,snd_sof_pci_intel_tgl
snd_sof_xtensa_dsp     12288  1 snd_sof_intel_hda_common
snd_sof               352256  3 snd_sof_pci,snd_sof_intel_hda_common,snd_sof_intel_hda
snd_sof_utils          16384  1 snd_sof
x86_pkg_temp_thermal    16384  0
snd_soc_hdac_hda       24576  1 snd_sof_intel_hda_common
intel_powerclamp       24576  0
snd_hda_ext_core       36864  4 snd_sof_intel_hda_common,snd_soc_hdac_hda,snd_sof_intel_hda_mlink,snd_sof_intel_hda
snd_soc_acpi_intel_match    90112  2 snd_sof_intel_hda_common,snd_sof_pci_intel_tgl
coretemp               24576  0
snd_soc_acpi           16384  2 snd_soc_acpi_intel_match,snd_sof_intel_hda_common
snd_intel_dspcfg       36864  2 snd_sof,snd_sof_intel_hda_common
snd_intel_sdw_acpi     16384  2 snd_sof_intel_hda_common,snd_intel_dspcfg
snd_hda_codec         208896  2 snd_soc_hdac_hda,snd_sof_intel_hda
kvm_intel             471040  0
snd_hda_core          143360  5 snd_hda_ext_core,snd_hda_codec,snd_sof_intel_hda_common,snd_soc_hdac_hda,snd_sof_intel_hda
snd_hwdep              20480  1 snd_hda_codec
pmt_telemetry          12288  0
soundwire_bus         110592  3 soundwire_intel,soundwire_generic_allocation,soundwire_cadence
nls_iso8859_1          12288  2
mei_hdcp               28672  0
mei_pxp                16384  0
intel_rapl_msr         20480  0
pmt_class              16384  1 pmt_telemetry
snd_soc_core          446464  4 soundwire_intel,snd_sof,snd_sof_intel_hda_common,snd_soc_hdac_hda
kvm                  1384448  1 kvm_intel
snd_compress           24576  1 snd_soc_core
ac97_bus               12288  1 snd_soc_core
snd_pcm_dmaengine      16384  1 snd_soc_core
irqbypass              12288  1 kvm
snd_pcm               196608  9 snd_hda_codec,soundwire_intel,snd_sof,snd_sof_intel_hda_common,snd_compress,snd_soc_core,snd_sof_utils,snd_hda_core,snd_pcm_dmaengine
rapl                   20480  0
snd_seq_midi           24576  0
snd_seq_midi_event     16384  1 snd_seq_midi
intel_cstate           20480  0
snd_rawmidi            53248  1 snd_seq_midi
snd_seq                98304  2 snd_seq_midi,snd_seq_midi_event
hid_sensor_als         16384  1
hid_sensor_magn_3d     20480  1
hid_sensor_gyro_3d     16384  0
hid_sensor_accel_3d    16384  1
hid_sensor_rotation    16384  0
hid_sensor_incl_3d     16384  0
hid_sensor_prox        16384  0
processor_thermal_device_pci    16384  0
snd_seq_device         16384  3 snd_seq,snd_seq_midi,snd_rawmidi
hid_sensor_trigger     20480  19 hid_sensor_gyro_3d,hid_sensor_prox,hid_sensor_incl_3d,hid_sensor_als,hid_sensor_accel_3d,hid_sensor_magn_3d,hid_sensor_rotation
processor_thermal_device    20480  1 processor_thermal_device_pci
snd_timer              49152  2 snd_seq,snd_pcm
industrialio_triggered_buffer    12288  1 hid_sensor_trigger
processor_thermal_rfim    32768  1 processor_thermal_device
cmdlinepart            12288  0
kfifo_buf              12288  1 industrialio_triggered_buffer
processor_thermal_mbox    16384  2 processor_thermal_rfim,processor_thermal_device
mei_me                 53248  2
snd                   143360  10 snd_seq,snd_seq_device,snd_hwdep,snd_hda_codec,snd_sof,snd_timer,snd_compress,snd_soc_core,snd_pcm,snd_rawmidi
processor_thermal_rapl    16384  1 processor_thermal_device
ucsi_acpi              12288  0
spi_nor               135168  0
hid_sensor_iio_common    28672  8 hid_sensor_gyro_3d,hid_sensor_trigger,hid_sensor_prox,hid_sensor_incl_3d,hid_sensor_als,hid_sensor_accel_3d,hid_sensor_magn_3d,hid_sensor_rotation
typec_ucsi             61440  1 ucsi_acpi
intel_rapl_common      36864  2 intel_rapl_msr,processor_thermal_rapl
industrialio          131072  13 industrialio_triggered_buffer,hid_sensor_gyro_3d,hid_sensor_trigger,hid_sensor_prox,hid_sensor_incl_3d,kfifo_buf,hid_sensor_als,hid_sensor_accel_3d,hid_sensor_magn_3d,hid_sensor_rotation
wmi_bmof               12288  0
joydev                 32768  0
input_leds             12288  0
mtd                   102400  3 spi_nor,cmdlinepart
8250_dw                20480  0
soundcore              16384  1 snd
mei                   167936  5 mei_hdcp,mei_pxp,mei_me
int340x_thermal_zone    16384  1 processor_thermal_device
typec                 110592  1 typec_ucsi
igen6_edac             24576  0
intel_vsec             20480  0
ov13858                24576  0
intel_skl_int3472_tps68470    20480  0
v4l2_fwnode            32768  1 ov13858
tps68470_regulator     12288  0
clk_tps68470           12288  0
v4l2_async             28672  2 v4l2_fwnode,ov13858
videodev              364544  3 v4l2_async,v4l2_fwnode,ov13858
mc                     86016  3 v4l2_async,videodev,ov13858
int3400_thermal        24576  0
intel_hid              28672  0
intel_skl_int3472_discrete    24576  0
acpi_thermal_rel       20480  1 int3400_thermal
mac_hid                12288  0
acpi_pad              184320  0
sparse_keymap          12288  1 intel_hid
acpi_tad               20480  0
sch_fq_codel           24576  3
msr                    12288  0
parport_pc             53248  0
dm_multipath           45056  0
ppdev                  24576  0
scsi_dh_rdac           16384  0
scsi_dh_emc            12288  0
lp                     28672  0
scsi_dh_alua           20480  0
parport                77824  3 parport_pc,lp,ppdev
pstore_blk             16384  0
ramoops                32768  0
reed_solomon           28672  1 ramoops
pstore_zone            32768  1 pstore_blk
efi_pstore             12288  0
drm                   761856  0
ip_tables              32768  0
x_tables               65536  5 xt_conntrack,nft_compat,xt_addrtype,ip_tables,xt_MASQUERADE
autofs4                57344  2
btrfs                1937408  0
blake2b_generic        24576  0
raid10                 73728  0
raid456               188416  0
async_raid6_recov      20480  1 raid456
async_memcpy           16384  2 raid456,async_raid6_recov
async_pq               20480  2 raid456,async_raid6_recov
async_xor              16384  3 async_pq,raid456,async_raid6_recov
async_tx               16384  5 async_pq,async_memcpy,async_xor,raid456,async_raid6_recov
xor                    20480  2 async_xor,btrfs
raid6_pq              126976  4 async_pq,btrfs,raid456,async_raid6_recov
libcrc32c              12288  5 nf_conntrack,nf_nat,btrfs,nf_tables,raid456
raid1                  61440  0
raid0                  24576  0
multipath              16384  0
linear                 16384  0
hid_sensor_custom      28672  0
hid_sensor_hub         28672  10 hid_sensor_gyro_3d,hid_sensor_trigger,hid_sensor_iio_common,hid_sensor_prox,hid_sensor_incl_3d,hid_sensor_als,hid_sensor_accel_3d,hid_sensor_magn_3d,hid_sensor_rotation,hid_sensor_custom
intel_ishtp_hid        28672  0
hid_generic            12288  0
usbhid                 77824  0
hid                   176128  4 usbhid,hid_sensor_hub,intel_ishtp_hid,hid_generic
spi_pxa2xx_platform    32768  0
dw_dmac                12288  0
dw_dmac_core           36864  1 dw_dmac
crct10dif_pclmul       12288  1
crc32_pclmul           12288  0
polyval_clmulni        12288  0
polyval_generic        12288  1 polyval_clmulni
ghash_clmulni_intel    16384  0
sha512_ssse3           49152  0
aesni_intel           356352  0
nvme                   57344  5
crypto_simd            16384  1 aesni_intel
i2c_i801               36864  0
spi_intel_pci          12288  0
spi_intel              32768  1 spi_intel_pci
i2c_smbus              16384  1 i2c_i801
cryptd                 24576  2 crypto_simd,ghash_clmulni_intel
intel_lpss_pci         24576  2
e1000e                348160  0
intel_ish_ipc          32768  0
intel_lpss             16384  1 intel_lpss_pci
nvme_core             204800  6 nvme
xhci_pci               24576  0
intel_ishtp            69632  2 intel_ishtp_hid,intel_ish_ipc
xhci_pci_renesas       16384  1 xhci_pci
idma64                 20480  0
nvme_common            28672  1 nvme_core
video                  73728  0
wmi                    40960  2 video,wmi_bmof
pinctrl_tigerlake      28672  1
